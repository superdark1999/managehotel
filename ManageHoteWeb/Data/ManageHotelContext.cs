﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ManageHoteWeb.Models;

namespace ManageHoteWeb.Data
{
    public class ManageHotelContext : DbContext
    {
        public ManageHotelContext(DbContextOptions<ManageHotelContext> options)
            : base(options)
        {
        }

        public DbSet<Room> Room { get; set; }
        public DbSet<RoomType> RoomType { get; set; }
        public DbSet<CustomerType> CustomerType { get; set; }
        public DbSet<Customer> Customer { get; set; }
        public DbSet<RentalForm> RentalForm { get; set; }
        public DbSet<Bill> Bill { get; set; }
        public DbSet<Surcharge> Surcharge { get; set; }
        public DbSet<TypeRoomReport> TypeRoomReport { get; set; }
        public DbSet<ListTypeRoomReport> ListTypeRoomReport { get; set; }
        public DbSet<RoomReport> RoomReport { get; set; }
        public DbSet<ListRoomReport> ListRoomReport { get; set; }


    }
}
