﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ManageHoteWeb.Migrations
{
    public partial class NullableBillId : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RentalForm_Bill_BillId",
                table: "RentalForm");

            migrationBuilder.AlterColumn<int>(
                name: "BillId",
                table: "RentalForm",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_RentalForm_Bill_BillId",
                table: "RentalForm",
                column: "BillId",
                principalTable: "Bill",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RentalForm_Bill_BillId",
                table: "RentalForm");

            migrationBuilder.AlterColumn<int>(
                name: "BillId",
                table: "RentalForm",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_RentalForm_Bill_BillId",
                table: "RentalForm",
                column: "BillId",
                principalTable: "Bill",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
