﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ManageHoteWeb.Migrations
{
    public partial class AddCustomerToRentalFormTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Customer_RentalForm_RentalFormid",
                table: "Customer");

            migrationBuilder.RenameColumn(
                name: "RentalFormid",
                table: "Customer",
                newName: "RentalFormId");

            migrationBuilder.RenameIndex(
                name: "IX_Customer_RentalFormid",
                table: "Customer",
                newName: "IX_Customer_RentalFormId");

            migrationBuilder.AlterColumn<int>(
                name: "RentalFormId",
                table: "Customer",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Customer_RentalForm_RentalFormId",
                table: "Customer",
                column: "RentalFormId",
                principalTable: "RentalForm",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Customer_RentalForm_RentalFormId",
                table: "Customer");

            migrationBuilder.RenameColumn(
                name: "RentalFormId",
                table: "Customer",
                newName: "RentalFormid");

            migrationBuilder.RenameIndex(
                name: "IX_Customer_RentalFormId",
                table: "Customer",
                newName: "IX_Customer_RentalFormid");

            migrationBuilder.AlterColumn<int>(
                name: "RentalFormid",
                table: "Customer",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_Customer_RentalForm_RentalFormid",
                table: "Customer",
                column: "RentalFormid",
                principalTable: "RentalForm",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
