﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ManageHoteWeb.Migrations
{
    public partial class DeletePriceInRoomTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Price",
                table: "Room");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Price",
                table: "Room",
                nullable: false,
                defaultValue: 0);
        }
    }
}
