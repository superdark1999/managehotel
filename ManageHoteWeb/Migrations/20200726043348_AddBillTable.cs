﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ManageHoteWeb.Migrations
{
    public partial class AddBillTable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BillId",
                table: "RentalForm",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Bill",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    NameCustomer = table.Column<string>(nullable: true),
                    SumPrice = table.Column<int>(nullable: false),
                    DayPayment = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bill", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RentalForm_BillId",
                table: "RentalForm",
                column: "BillId");

            migrationBuilder.AddForeignKey(
                name: "FK_RentalForm_Bill_BillId",
                table: "RentalForm",
                column: "BillId",
                principalTable: "Bill",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_RentalForm_Bill_BillId",
                table: "RentalForm");

            migrationBuilder.DropTable(
                name: "Bill");

            migrationBuilder.DropIndex(
                name: "IX_RentalForm_BillId",
                table: "RentalForm");

            migrationBuilder.DropColumn(
                name: "BillId",
                table: "RentalForm");
        }
    }
}
