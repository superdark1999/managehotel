﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace ManageHoteWeb.Migrations
{
    public partial class AddRentalFormTab : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "RentalFormid",
                table: "Customer",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "RentalForm",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RoomId = table.Column<int>(nullable: false),
                    DayRent = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RentalForm", x => x.id);
                    table.ForeignKey(
                        name: "FK_RentalForm_Room_RoomId",
                        column: x => x.RoomId,
                        principalTable: "Room",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Customer_RentalFormid",
                table: "Customer",
                column: "RentalFormid");

            migrationBuilder.CreateIndex(
                name: "IX_RentalForm_RoomId",
                table: "RentalForm",
                column: "RoomId");

            migrationBuilder.AddForeignKey(
                name: "FK_Customer_RentalForm_RentalFormid",
                table: "Customer",
                column: "RentalFormid",
                principalTable: "RentalForm",
                principalColumn: "id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Customer_RentalForm_RentalFormid",
                table: "Customer");

            migrationBuilder.DropTable(
                name: "RentalForm");

            migrationBuilder.DropIndex(
                name: "IX_Customer_RentalFormid",
                table: "Customer");

            migrationBuilder.DropColumn(
                name: "RentalFormid",
                table: "Customer");
        }
    }
}
