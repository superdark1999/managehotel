﻿using ManageHoteWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManageHoteWeb.ViewModels
{
    public class DetailBillViewModel
    {
        public Bill Bill { get; set; }
        public List<RentalRoomInBillView> rentalRoomInBillView { get; set; }
    }

    public class RentalRoomInBillView 
    {
        public string Room { get; set; }
        public int NumberOfDayRent { get; set; }
        public int Price { get; set; }
        public int SumPrice { get; set; }
    }
}
