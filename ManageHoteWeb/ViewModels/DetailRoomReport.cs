﻿using ManageHoteWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManageHoteWeb.ViewModels
{
    public class DetailRoomReport
    {
        public ListRoomReport ListRoomReport { get; set; }
        public List<RoomReport> RoomReports { get; set; }
    }
}
