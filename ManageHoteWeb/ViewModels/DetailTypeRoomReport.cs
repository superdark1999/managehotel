﻿using ManageHoteWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManageHoteWeb.ViewModels
{
    public class DetailTypeRoomReport
    {
        public ListTypeRoomReport ListTypeRoomReport { get; set; }
        public List<TypeRoomReport> TypeRoomReports { get; set; }
    }
}
