﻿using ManageHoteWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManageHoteWeb.ViewModels
{
    public class DetailsRentalFormViewModel
    {
        public RentalForm RentalForm { get; set; }
        public IEnumerable<Customer> Customers { get; set; }
    }
}
