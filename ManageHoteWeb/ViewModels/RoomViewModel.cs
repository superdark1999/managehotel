﻿using ManageHoteWeb.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManageHoteWeb.ViewModels
{
    public class RoomViewModel
    {
        public IEnumerable<RoomType> RoomTypes { get; set; }
        public Room Room { get; set; }
    }
}
