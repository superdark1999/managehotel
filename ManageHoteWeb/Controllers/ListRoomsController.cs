﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ManageHoteWeb.Data;
using ManageHoteWeb.Models;

namespace ManageHoteWeb.Controllers
{
    public class ListRoomsController : Controller
    {
        private readonly ManageHotelContext _context;

        public ListRoomsController(ManageHotelContext context)
        {
            _context = context;
        }

        // GET: ListRooms
        public async Task<IActionResult> Index(string room)
        {
            if (!String.IsNullOrEmpty(room))
            {
                return View(await _context.Room.Include(r => r.RoomType)
                    .Where(r => r.Name.Contains(room))
                    .OrderByDescending(r => r.Name).ToListAsync());
            }
            return View(await _context.Room.Include(r => r.RoomType)
                   .OrderByDescending(r => r.Name).ToListAsync());
        }

        private bool RoomExists(int id)
        {
            return _context.Room.Any(e => e.Id == id);
        }
    }
}
