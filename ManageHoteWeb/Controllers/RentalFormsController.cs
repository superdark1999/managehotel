﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ManageHoteWeb.Data;
using ManageHoteWeb.Models;
using ManageHoteWeb.ViewModels;

namespace ManageHoteWeb.Controllers
{
    public class RentalFormsController : Controller
    {
        private readonly ManageHotelContext _context;

        public RentalFormsController(ManageHotelContext context)
        {
            _context = context;
        }

        // GET: RentalForms
        public async Task<IActionResult> Index()
        {
            var manageHotelContext = await _context.RentalForm.Include(r => r.Room)
                .OrderByDescending(r => r.DayRent)
                .ToListAsync();
            return View(manageHotelContext);
        }

        // GET: RentalForms/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rentalForm = await _context.RentalForm
                .Include(r => r.Room)
                .FirstOrDefaultAsync(m => m.id == id);
            if (rentalForm == null)
            {
                return NotFound();
            }

            var viewModel = new DetailsRentalFormViewModel
            {
                RentalForm = rentalForm,
                Customers = _context.Customer.Include(c => c.CustomerType).Where(c => c.RentalFormId == rentalForm.id).ToList()
            };
            return View(viewModel);
        }

        // GET: RentalForms/Create
        public IActionResult Create()
        {
            ViewData["RoomName"] = new SelectList(_context.Room, "Id", "Name");
            return View();
        }

        // POST: RentalForms/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,RoomId,DayRent")] RentalForm rentalForm)
        {
            if (ModelState.IsValid)
            {
                //rentalForm.BillId = 0;
                _context.Add(rentalForm);
                await _context.SaveChangesAsync();

                //TempData["RentalFormId"] = rentalForm.id;
                //return RedirectToAction("index", "Customers");

                return Redirect($"/Customers?id={rentalForm.id}");
            }
            ViewData["RoomId"] = new SelectList(_context.Room, "Id", "Name", rentalForm.RoomId);
            return View(rentalForm);
        }

        // GET: RentalForms/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rentalForm = await _context.RentalForm.FindAsync(id);
            if (rentalForm == null)
            {
                return NotFound();
            }
            ViewData["RoomId"] = new SelectList(_context.Room, "Id", "Name", rentalForm.RoomId);
            return View(rentalForm);
        }

        // POST: RentalForms/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("id,RoomId,DayRent")] RentalForm rentalForm)
        {
            if (id != rentalForm.id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(rentalForm);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RentalFormExists(rentalForm.id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["RoomId"] = new SelectList(_context.Room, "Id", "Id", rentalForm.RoomId);
            return View(rentalForm);
        }

        // GET: RentalForms/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var rentalForm = await _context.RentalForm
                .Include(r => r.Room)
                .FirstOrDefaultAsync(m => m.id == id);
            if (rentalForm == null)
            {
                return NotFound();
            }

            return View(rentalForm);
        }

        // POST: RentalForms/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var rentalForm = await _context.RentalForm.FindAsync(id);
            _context.RentalForm.Remove(rentalForm);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RentalFormExists(int id)
        {
            return _context.RentalForm.Any(e => e.id == id);
        }
    }
}
