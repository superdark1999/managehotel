﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ManageHoteWeb.Data;
using ManageHoteWeb.Models;
using Microsoft.AspNetCore.Routing;

namespace ManageHoteWeb.Controllers
{
    public class CustomersController : Controller
    {
        private readonly ManageHotelContext _context;

        public CustomersController(ManageHotelContext context)
        {
            _context = context;
        }

        // GET: Customers
        public async Task<IActionResult> Index(int id)
        {
            var manageHotelContext = _context.Customer
                .Include(c => c.CustomerType)
                .Where(c => c.RentalFormId == id);
            ViewData["RentalFormId"] = id;
            return View(await manageHotelContext.ToListAsync());
        }

        // GET: Customers/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var customer = await _context.Customer
                .Include(c => c.CustomerType)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (customer == null)
            {
                return NotFound();
            }

            return View(customer);
        }

        // GET: Customers/Create
        public IActionResult Create(int id)
        {
            ViewData["CustomerTypeId"] = new SelectList(_context.CustomerType, "Id", "Name");
            ViewData["RentalFormId"] = id;
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,IdCard,Address,CustomerTypeId,RentalFormId")] Customer customer)
        {
            if (ModelState.IsValid)
            {
                var rentalForm = _context.RentalForm.SingleOrDefault(r => r.id == customer.RentalFormId);
                var room = _context.Room.SingleOrDefault(r => r.Id == rentalForm.RoomId);

                var numberOfCustomerNow = _context.Customer.Where(c => c.RentalFormId == customer.RentalFormId).ToList();
                if (numberOfCustomerNow.Count < room.MaxPerson)
                {
                    _context.Add(customer);
                    await _context.SaveChangesAsync();
                }
                return RedirectToAction(nameof(Index), new RouteValueDictionary(
                new { controller = "Customers",  Id = customer.RentalFormId }
                )
                );
            }
            ViewData["CustomerTypeId"] = new SelectList(_context.CustomerType, "Id", "Name", customer.CustomerTypeId);
            ViewData["RentalFormId"] = new SelectList(_context.RentalForm, "id", "id", customer.RentalFormId);
            return View(customer);
        }

        // GET: Customers/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var customer = await _context.Customer.FindAsync(id);
            if (customer == null)
            {
                return NotFound();
            }
            ViewData["CustomerTypeId"] = new SelectList(_context.CustomerType, "Id", "Name", customer.CustomerTypeId);
            ViewData["RentalFormId"] = customer.RentalFormId;
            return View(customer);
        }

        // POST: Customers/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,IdCard,Address,CustomerTypeId,RentalFormId")] Customer customer)
        {
            if (id != customer.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(customer);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!CustomerExists(customer.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index), new RouteValueDictionary(
                new { controller = "Customers", Id = customer.RentalFormId }
                )
                );
            }
            ViewData["CustomerTypeId"] = new SelectList(_context.CustomerType, "Id", "Id", customer.CustomerTypeId);
            ViewData["RentalFormId"] = new SelectList(_context.RentalForm, "id", "id", customer.RentalFormId);
            return View(customer);
        }

        // GET: Customers/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var customer = await _context.Customer
                .Include(c => c.CustomerType)
                .Include(c => c.RentalForm)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (customer == null)
            {
                return NotFound();
            }

            ViewData["RentalFormId"] = customer.RentalFormId;
            return View(customer);
        }

        // POST: Customers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var customer = await _context.Customer.FindAsync(id);
            var rentalId = customer.RentalFormId;
            _context.Customer.Remove(customer);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index), new RouteValueDictionary(
                new { controller = "Customers", Id =  rentalId}
                )
                );
        }

        private bool CustomerExists(int id)
        {
            return _context.Customer.Any(e => e.Id == id);
        }
    }
}
