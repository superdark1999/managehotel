﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ManageHoteWeb.Data;
using ManageHoteWeb.Models;
using ManageHoteWeb.ViewModels;

namespace ManageHoteWeb.Controllers
{
    public class ListTypeRoomReportsController : Controller
    {
        private readonly ManageHotelContext _context;

        public ListTypeRoomReportsController(ManageHotelContext context)
        {
            _context = context;
        }

        // GET: ListTypeRoomReports
        public async Task<IActionResult> Index()
        {
            return View(await _context.ListTypeRoomReport.ToListAsync());
        }

        // GET: ListTypeRoomReports/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var listTypeRoomReport = await _context.ListTypeRoomReport
                .FirstOrDefaultAsync(m => m.Id == id);
            if (listTypeRoomReport == null)
            {
                return NotFound();
            }

            var viewModel = new DetailTypeRoomReport()
            {
                ListTypeRoomReport = listTypeRoomReport,
                TypeRoomReports = _context.TypeRoomReport.Where(t => t.ListReportIndex == listTypeRoomReport.Id).ToList()

            };

            return View(viewModel);
        }

        // GET: ListTypeRoomReports/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ListTypeRoomReports/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Month")] ListTypeRoomReport listTypeRoomReport)
        {
            if (ModelState.IsValid)
            {
                _context.Add(listTypeRoomReport);
                await _context.SaveChangesAsync();

                var listTypeRoom = _context.RoomType.ToList();

                var sumRevenue = _context.RentalForm.Where(r => r.DayRent.Month == listTypeRoomReport.Month).Select(r => r.SumPrice).Sum();
                foreach(var typeRoom in listTypeRoom)
                {
                    var typeRoomReport = new TypeRoomReport();

                    var listRentalFormWithTypeRoom = _context.RentalForm
                   .Where(r => r.DayRent.Month == listTypeRoomReport.Month && r.Room.RoomTypeId == typeRoom.Id);

                    typeRoomReport.ListReportIndex = listTypeRoomReport.Id;
                    typeRoomReport.TypeRoom = typeRoom.Name;
                    typeRoomReport.Revenue = listRentalFormWithTypeRoom.Select(r =>r.SumPrice).Sum();
                    if (typeRoomReport.Revenue != 0)
                        typeRoomReport.Rate = Math.Round(((double)typeRoomReport.Revenue / (double)sumRevenue) * 100, 2);
                    _context.Add(typeRoomReport);
                    await _context.SaveChangesAsync();
                }

                return Redirect($"/ListTypeRoomReports/Details/{listTypeRoomReport.Id}");
            }
            return View(listTypeRoomReport);
        }

        // GET: ListTypeRoomReports/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var listTypeRoomReport = await _context.ListTypeRoomReport.FindAsync(id);
            if (listTypeRoomReport == null)
            {
                return NotFound();
            }
            return View(listTypeRoomReport);
        }

        // POST: ListTypeRoomReports/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Month")] ListTypeRoomReport listTypeRoomReport)
        {
            if (id != listTypeRoomReport.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(listTypeRoomReport);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ListTypeRoomReportExists(listTypeRoomReport.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(listTypeRoomReport);
        }

        // GET: ListTypeRoomReports/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var listTypeRoomReport = await _context.ListTypeRoomReport
                .FirstOrDefaultAsync(m => m.Id == id);
            if (listTypeRoomReport == null)
            {
                return NotFound();
            }

            return View(listTypeRoomReport);
        }

        // POST: ListTypeRoomReports/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var listTypeRoomReport = await _context.ListTypeRoomReport.FindAsync(id);
            _context.ListTypeRoomReport.Remove(listTypeRoomReport);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ListTypeRoomReportExists(int id)
        {
            return _context.ListTypeRoomReport.Any(e => e.Id == id);
        }
    }
}
