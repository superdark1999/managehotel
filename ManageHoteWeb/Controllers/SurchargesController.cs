﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ManageHoteWeb.Data;
using ManageHoteWeb.Models;

namespace ManageHoteWeb.Controllers
{
    public class SurchargesController : Controller
    {
        private readonly ManageHotelContext _context;

        public SurchargesController(ManageHotelContext context)
        {
            _context = context;
        }

        // GET: Surcharges
        public async Task<IActionResult> Index()
        {
            return View(await _context.Surcharge.ToListAsync());
        }


        // GET: Surcharges/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var surcharge = await _context.Surcharge.FindAsync(id);
            if (surcharge == null)
            {
                return NotFound();
            }
            return View(surcharge);
        }

        // POST: Surcharges/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,NumberPeopleForSurcharge,Coefficent")] Surcharge surcharge)
        {
            if (id != surcharge.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(surcharge);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SurchargeExists(surcharge.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(surcharge);
        }

        private bool SurchargeExists(int id)
        {
            return _context.Surcharge.Any(e => e.Id == id);
        }
    }
}
