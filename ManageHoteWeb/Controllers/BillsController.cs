﻿using ManageHoteWeb.Data;
using ManageHoteWeb.Models;
using ManageHoteWeb.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManageHoteWeb.Controllers
{
    public class BillsController : Controller
    {
        private readonly ManageHotelContext _context;

        public BillsController(ManageHotelContext context)
        {
            _context = context;
        }

        // GET: Bills
        public async Task<IActionResult> Index()
        {
            return View(await _context.Bill.ToListAsync());
        }

        // GET: Bills/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bill = await _context.Bill
                .FirstOrDefaultAsync(m => m.Id == id);
            if (bill == null)
            {
                return NotFound();
            }

            var listRentalForm = _context.RentalForm.Include(r => r.Room.RoomType).Where(r => r.BillId == bill.Id).ToList();

            var viewModel = new DetailBillViewModel
            {
                Bill = bill,
                rentalRoomInBillView = new List<RentalRoomInBillView>()
            };

            if (listRentalForm.Count == 0)
                return View(viewModel);

            foreach (var rentalForm in bill.rentalForms)
            {
                var view = new RentalRoomInBillView();
                view.Room = rentalForm.Room.Name;
                view.NumberOfDayRent = rentalForm.NumberOfDayRental;
                view.Price = rentalForm.Room.RoomType.Price;

                rentalForm.Customers = _context.Customer.Include(c => c.CustomerType).Where(c => c.RentalFormId == rentalForm.id).ToList();
                view.SumPrice = rentalForm.SumPrice;

                viewModel.rentalRoomInBillView.Add(view);
            };

            return View(viewModel);
        }

        // GET: Bills/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Bills/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        public async Task<IActionResult> Create(string customerName, string address ,IEnumerable<string> values)
        {
            Bill bill = new Bill
            {
                NameCustomer = customerName,
                Address = address,
                DayPayment = DateTime.Now,
            };

            _context.Add(bill);
            await _context.SaveChangesAsync();

            foreach(var item in values)
            {
                var rentalForm = _context.RentalForm
                    .Include(r=>r.Room.RoomType)
                    .Where(r => r.Room.Name == item)
                    .OrderByDescending(r=> r.DayRent)
                    .First();

                rentalForm.BillId = bill.Id;

                var listCustomerOfRentalForm = _context.Customer.Include(c => c.CustomerType).Where(c => c.RentalFormId == rentalForm.id).ToList();
                var numberOfDayRent = CalculateNumberOfDayRent(rentalForm.DayRent, bill.DayPayment);
                rentalForm.NumberOfDayRental = numberOfDayRent;
                rentalForm.SumPrice = CalculatePriceInRentalForm(numberOfDayRent, rentalForm.Room.RoomType.Price, listCustomerOfRentalForm);
                bill.SumPrice += rentalForm.SumPrice;

                _context.Update(rentalForm);
                await _context.SaveChangesAsync();
            }

            _context.Update(bill);
            await _context.SaveChangesAsync();

            return Json("Success");
        }

        // GET: Bills/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bill = await _context.Bill.FindAsync(id);
            if (bill == null)
            {
                return NotFound();
            }
            return View(bill);
        }

        // POST: Bills/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,NameCustomer,SumPrice,DayPayment")] Bill bill)
        {
            if (id != bill.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(bill);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!BillExists(bill.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(bill);
        }

        // GET: Bills/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var bill = await _context.Bill
                .FirstOrDefaultAsync(m => m.Id == id);
            if (bill == null)
            {
                return NotFound();
            }

            var listRentalFormOfBill = _context.RentalForm.Where(r => r.BillId == bill.Id).ToList();
            
            if (listRentalFormOfBill.Count == 0)
                return View(bill);

            foreach (var rentalForm in listRentalFormOfBill)
            {
                rentalForm.BillId = null;
                _context.Update(rentalForm);
            }
            await _context.SaveChangesAsync();

            return View(bill);
        }

        // POST: Bills/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var bill = await _context.Bill.FindAsync(id);
            _context.Bill.Remove(bill);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool BillExists(int id)
        {
            return _context.Bill.Any(e => e.Id == id);
        }

        public int CalculatePriceInRentalForm(int numberOfDay, int Price, List<Customer> customers)
        {
            var surchargeDb = _context.Surcharge.First();
                
            var surcharge  = customers.Count >= surchargeDb.NumberPeopleForSurcharge ? surchargeDb.Coefficent : 1;

            var coefficent = customers.Select(c => c.CustomerType.Coefficent).Max();

            return (int)(numberOfDay * Price * coefficent * surcharge);
        }

        public int CalculateNumberOfDayRent(DateTime start, DateTime end)
        {
            return (end - start ).TotalDays >= 1 ? (int)(end - start).TotalDays : 1;
        }
    }
}
