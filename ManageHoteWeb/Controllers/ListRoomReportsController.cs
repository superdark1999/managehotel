﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using ManageHoteWeb.Data;
using ManageHoteWeb.Models;
using ManageHoteWeb.ViewModels;

namespace ManageHoteWeb.Controllers
{
    public class ListRoomReportsController : Controller
    {
        private readonly ManageHotelContext _context;

        public ListRoomReportsController(ManageHotelContext context)
        {
            _context = context;
        }

        // GET: ListRoomReports
        public async Task<IActionResult> Index()
        {
            return View(await _context.ListRoomReport.ToListAsync());
        }

        // GET: ListRoomReports/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var listRoomReport = await _context.ListRoomReport
                .FirstOrDefaultAsync(m => m.Id == id);
            if (listRoomReport == null)
            {
                return NotFound();
            }

            var viewModel = new DetailRoomReport
            {
                ListRoomReport = listRoomReport,
                RoomReports = _context.RoomReport.Where(r => r.ListRoomReport == listRoomReport.Id).ToList()
            };

            return View(viewModel);
        }

        // GET: ListRoomReports/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: ListRoomReports/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Month")] ListRoomReport listRoomReport)
        {
            if (ModelState.IsValid)
            {
                _context.Add(listRoomReport);
                await _context.SaveChangesAsync();

                var listRoom = _context.Room.ToList();

                var sumDayRent = _context.RentalForm.Where(r => r.DayRent.Month == listRoomReport.Month).Select(r => r.NumberOfDayRental).Sum();
                foreach (var room in listRoom)
                {
                    var roomReport = new RoomReport();

                    var listRentalFormWithRoom = _context.RentalForm
                   .Where(r => r.DayRent.Month == listRoomReport.Month && r.Room.Id == room.Id);

                    roomReport.ListRoomReport = listRoomReport.Id;
                    roomReport.Room = room.Name;
                    roomReport.NumberOfDayRental = listRentalFormWithRoom.Select(r => r.NumberOfDayRental).Sum();
                    if (roomReport.NumberOfDayRental != 0)
                        roomReport.Rate = Math.Round(((double)roomReport.NumberOfDayRental / (double)sumDayRent) * 100, 2);

                    _context.Add(roomReport);
                    await _context.SaveChangesAsync();
                }

                return Redirect($"/ListRoomReports/Details/{listRoomReport.Id}");
            }
            return View(listRoomReport);
        }

        // GET: ListRoomReports/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var listRoomReport = await _context.ListRoomReport.FindAsync(id);
            if (listRoomReport == null)
            {
                return NotFound();
            }
            return View(listRoomReport);
        }

        // POST: ListRoomReports/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Month")] ListRoomReport listRoomReport)
        {
            if (id != listRoomReport.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(listRoomReport);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!ListRoomReportExists(listRoomReport.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(listRoomReport);
        }

        // GET: ListRoomReports/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var listRoomReport = await _context.ListRoomReport
                .FirstOrDefaultAsync(m => m.Id == id);
            if (listRoomReport == null)
            {
                return NotFound();
            }

            return View(listRoomReport);
        }

        // POST: ListRoomReports/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var listRoomReport = await _context.ListRoomReport.FindAsync(id);
            _context.ListRoomReport.Remove(listRoomReport);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool ListRoomReportExists(int id)
        {
            return _context.ListRoomReport.Any(e => e.Id == id);
        }
    }
}
