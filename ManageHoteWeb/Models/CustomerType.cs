﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ManageHoteWeb.Models
{
    public class CustomerType
    {
        public int Id { get; set; }

        [Display(Name = "Loại khách")]
        public string Name { get; set; }

        [Display(Name = "Hệ số nhân")]
        public float Coefficent { get; set; }

    }
}
