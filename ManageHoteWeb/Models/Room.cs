﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ManageHoteWeb.Models
{
    public class Room
    {
        public int Id { get; set; }

        [Display(Name = "Tên phòng")]
        public string Name { get; set; }

        public int RoomTypeId { get; set; }

        public RoomType RoomType { get; set; }

        [Display(Name = "Tình trạng")]
        public string Status { get; set; }

        [Display(Name = "Số người tối đa")]
        public int MaxPerson { get; set; }

        [Display(Name = "Ghi chú")]

        public string Note { get; set; }

    }
}
