﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ManageHoteWeb.Models
{
    public class Bill
    {
        public int Id { get; set; }

        [Display(Name = "Khách hàng")]
        public string NameCustomer { get; set; }

        [Display(Name ="Địa chỉ")]
        public string Address { get; set; }

        [Display(Name ="Tổng giá trị")]
        public int SumPrice { get; set; }

        [Display(Name ="Ngày thanh toán")]
        public DateTime DayPayment { get; set; }

        public List<RentalForm> rentalForms { get; set; }

    }
}
