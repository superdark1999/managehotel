﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManageHoteWeb.Models
{
    public class RoomReport
    {
        public int Id { get; set; }
        public string Room { get; set; }
        public int NumberOfDayRental { get; set; }
        public double Rate { get; set; }
        public int ListRoomReport { get; set; }

    }
}
