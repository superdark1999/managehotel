﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ManageHoteWeb.Models
{
    public class Surcharge
    {
        public int Id { get; set; }

        [Display(Name ="Số người tối thiểu cho phụ thu")]
        public int NumberPeopleForSurcharge { get; set; }

        [Display(Name = "Hệ số phụ thu")]
        public double Coefficent { get; set; }
    }
}
