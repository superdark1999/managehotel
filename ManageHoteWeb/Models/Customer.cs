﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;

namespace ManageHoteWeb.Models
{
    public class Customer
    {
        public int Id { get; set; }

        [Display(Name = "Tên khách hàng")]
        public string Name { get; set; }

        [Display(Name = "Chứng minh nhân dân")]
        public string IdCard { get; set; }

        [Display(Name = "Địa chỉ")]
        public string Address { get; set; }

        public int CustomerTypeId { get; set; }

        public CustomerType CustomerType { get; set; }

        public int RentalFormId { get; set; }
        public RentalForm RentalForm { get; set; }

    }
}
