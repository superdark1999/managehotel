﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ManageHoteWeb.Models
{
    public class TypeRoomReport
    {
        public int Id { get; set; }
        public string TypeRoom { get; set; }
        public int Revenue { get; set; }
        public double Rate  { get; set; }
        public int ListReportIndex { get; set; }
    }
}
