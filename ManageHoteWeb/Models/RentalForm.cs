﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;

namespace ManageHoteWeb.Models
{
    public class RentalForm
    {
        public int id { get; set; }
        public int RoomId { get; set; }
        public Room Room { get; set; }

        public List<Customer> Customers { get; set; }

        [Display(Name = "Ngày thuê")]
        public DateTime DayRent { get; set; }
        public int? BillId { get; set; }
        public virtual Bill Bill { get; set; }

        public int SumPrice { get; set; }

        public int NumberOfDayRental { get; set; }
    }
}
