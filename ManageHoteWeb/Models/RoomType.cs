﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ManageHoteWeb.Models
{
    public class RoomType
    {
        public int Id { get; set; }

        [Display(Name = "Loại phòng")]
        public string Name { get; set; }

        [Display(Name = "Giá")]
        public int Price { get; set; }

    }
}
