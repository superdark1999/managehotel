﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ManageHoteWeb.Models
{
    public class ListTypeRoomReport
    {
        public int Id { get; set; }

        [Display(Name ="Tháng")]
        [Range(1, 12, ErrorMessage ="Kiểm tra lại thông tin tháng")]
        public int Month { get; set; }

    }
}
